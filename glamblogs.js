
// COLLECTIONS

Blogs = new Meteor.Collection('blogs');

Meteor.users.deny({
  update: function() {
    return true;
  },
  insert: function() {
  return true;
  },
});

// IRON ROUTER

Router.configure({
  layoutTemplate:'main',
  loadingTemplate: 'loading'
});

// on startup if there are no users registered we use this page
Router.route('/startup', {
  name: 'startup',
  template: 'startup',
  onBeforeAction: function(){
    // check if there are users registered
    if (Meteor.users.find().count() === 0) {
      this.next();
    } else {
       Router.go('home');
    }
  }
});

Router.route('/', {
  name: 'home',
  template: 'home',
  onBeforeAction: function(){
    var rT = Session.get('resetToken');
    if (rT) {
      Router.go('reset');
    } else {
      this.next();
    }
  }
});

Router.route('/reset', {
  name: 'reset',
  template: 'reset',
  data: function(){
    return Meteor.user();
  },
  onBeforeAction: function(){
    var rT = Session.get('resetToken');
    var currentUser = Meteor.user();
    // if there's no reset token, redirect to homepage
    if (!rT) {
      Router.go('/');
    } 
    // if they have a token, go to the verified page
    this.next();
  } 
});

Router.route('/registerBlog');
Router.route('/success');
Router.route('/findBlogs');
Router.route('/login');
Router.route('/forgot');

Router.route('/admin', {
  name: 'admin',
  template: 'admin',
  data: function(){
    return Meteor.user();
  },
  onBeforeAction: function(){
    var currentUser = Meteor.user();
    if (currentUser){
      this.next();
    } else {
      Router.go('login');
    }
  }
});

Router.route('/register', {
  name: 'register',
  template: 'register',
  data: function(){
    return Meteor.user();
  },
  onBeforeAction: function(){
    var currentUser = Meteor.user();
    if (currentUser){
      this.next();
    } else {
      this.render('login');
    }
  }
});
