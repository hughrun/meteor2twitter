# Meteor2Twitter
This is a Meteor app that takes RSS feeds, stores them in a Mongo database and tweets when a new feed is added or a new post is published.

It loops every 10 minutes, and only announces one new feed each cycle, to avoid triggering Twitter’s spam blockers.

Based on [rss-twitter-bot](https://gitlab.com/hughr/rss-twitter-bot).

**Note this repo is no longer being maintained. Get the latest version [on GitHub](https://github.com/hughrun/CommunityTweets)**

# Requirements
* [nodejs](https://nodejs.org)
* [meteorjs](https://www.meteor.com/)
* a [Twitter account](https://apps.twitter.com) with app keys
* a [Mailgun](https://www.mailgun.com) account

# Meteor dependencies
You will need to install the following packages:
* accounts-password
* akasha:request
* danimal:feedparser
* email
* iron:router
* mrt:twit
* themeteorchef:jquery-validation

# Demo
See [the newCardigan GLAMblogs app](https://glamblogs.newcardigan.org) for a live example.

# Future features
I plan to do some more development on this in future. Planned features include:
* adding a page loading spinner
* fixes to the admin interface
* ability to reject an over-write suggestion without deleting the original listing
* download-opml-file function to get all (or a subset of) feeds as an OPML file

# License
MIT

